package com.spring.test.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.spring.test.name.Name;
import com.spring.test.name.NameWrapper;

@RestController
@RequestMapping("west")
public class WestController {
	
	private static List<Name> names;
	
	@RequestMapping("/")
	public String index() {
		names = new ArrayList<Name>();
		return "You have reached the index";
	}
	
	@GetMapping("/getMyName/{firstName}/{lastName}")
	public NameWrapper getName(@PathVariable String firstName, @PathVariable String lastName) {
		Name name = new Name(firstName,lastName);
		int index = names.indexOf(name);
		
		NameWrapper nw = new NameWrapper();
		
		if(index != -1) {
			nw.setSuccess(true);
			nw.setName(names.get(index));
		} else {
			nw.setError("Name not found");
		}
		
		return nw;
	}
	
	@PostMapping("/setMyName")
	public Name setName(@RequestBody Name name) {
		System.out.println(name.toString());
		
		name.setLastName("Druples");
		names.add(name);
		return name;
	}
	
	@RequestMapping(value = "/addName")
    public NameWrapper addName (@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName) {
		
		NameWrapper nw = new NameWrapper();
		if(firstName != null && lastName != null) {
			Name name = new Name(firstName, lastName);
			names.add(name);
			nw.setName(name);
			nw.setSuccess(true);
		} else {
			nw.setSuccess(false);
			nw.setError("Error: both first and last name should be non-null");
		}
        
        return nw;
    }
	
}
