package com.spring.test.controllers;

public class Book {
	private String isbn;
	private String title;
	private String author;
	private int pages;
	private boolean isTwilight;
	
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	public boolean isTwilight() {
		return isTwilight;
	}
	public void setTwilight(boolean isTwilight) {
		this.isTwilight = isTwilight;
	}
	
	@Override
	public String toString() {
		return "Book [isbn=" + isbn + ", title=" + title + ", author=" + author + ", pages=" + pages + ", isTwilight="
				+ isTwilight + "]";
	}
	
	
}
