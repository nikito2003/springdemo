package com.spring.test.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.spring.test.name.Name;
import com.spring.test.name.NameWrapper;

@Controller
@RequestMapping("test")
public class TestController {

	@RequestMapping("/")
	public String index() {
		return "index";
	}

	@PostMapping("/hello")
	public String sayHello(@RequestParam("name") String name, Model model) {
		model.addAttribute("name", name);
		return "hello";
	}

	@PostMapping("/test.do")
	@ResponseBody
	public String test() {
		System.out.println("Testing...");
		return "Hello world!";
	}

	@GetMapping("/request2/{firstName}/{lastName}")
	public String handler(@PathVariable String firstName, @PathVariable String lastName) {
		ModelAndView mv = new ModelAndView();
		mv.addObject("firstName", firstName);
		mv.addObject("lastName", lastName);
		return "name";
	}
	
	@PostMapping("/submitName")
	@ResponseBody
	public NameWrapper submitName(@RequestBody Name name) {
		NameWrapper nw = new NameWrapper();
		nw.setName(name);
		
		return nw;
	}
}
