function getMyName() {
	var request = $.ajax({
		type:"get",
		url: "/west/getMyName"
	});

	request.done(function( msg ) {
		alert( msg.firstName + msg.lastName );
		console.log(msg);
		console.log(msg.firstName);
		console.log(msg.lastName);
	});

	request.fail(function( jqXHR, textStatus ) {
		alert( "FAIL: " + textStatus );
	});
}

function setMyName() {
	
	var name = {
			"firstName" : "Anand",
			"lastName" : "Pradeep"
	};
	
	var nameData = JSON.stringify(name);
	
	var request = $.ajax({
		type:"post",
		url: "/west/setMyName",
		data : nameData,
		contentType : "application/json",
		accept: "application/json"
	});

	request.done(function( msg ) {
		alert( msg.firstName + msg.lastName );
		console.log(msg.firstName);
		console.log(msg.lastName);
	});

	request.fail(function( jqXHR, textStatus ) {
		console.log(jqXHR);
		console.log(textStatus);
		alert(jqXHR.responseText);
		alert( "FAIL: " + textStatus );
	});
}

function addName() {
	
	var name = {
			"firstName" : "Anand",
			"lastName" : "Pradeep"
	};
	
	var nameData = JSON.stringify(name);
	
	var request = $.ajax({
		type:"post",
		url: "/west/addName",
		data : nameData,
		contentType : "application/json",
		accept: "application/json"
	});

	request.done(function( msg ) {
		alert( msg );
	});

	request.fail(function( jqXHR, textStatus ) {
		console.log(jqXHR);
		console.log(textStatus);
		alert(jqXHR.responseText);
		alert( "FAIL: " + textStatus );
	});
}