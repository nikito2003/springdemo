<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<!-- Static content -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript" src="/resources/js/send.js"></script>
<script type="text/javascript" src="/resources/js/getMyName.js"></script>

<title>Spring Boot</title>
</head>
<body>
  <h1>Spring Boot - MVC web application example</h1>
  <hr>

  <div class="form">
    <form action="hello" method="post">
      <table>
        <tr>
          <td>Enter Your name</td>
          <td><input id="name" name="name"></td>
          <td><input type="submit" value="Submit"></td>
        </tr>
      </table>
    </form>
  </div>
  
  <div class="other">
  	<button onclick="send()">Send Me!</button>
  </div>
  
  <div class="other">
  	<button onclick="getMyName()">Get My Name!</button>
  </div>
  
  <div class="form">
    
      <table>
        <tr>
          <td>Enter Your name</td>
          <td><input id="firstName" name="firstName"></td>
          <td><input id="lastName" name="lastName"></td>
          <td><input type="submit" value="Submit" onclick="submitName();"></td>
        </tr>
      </table>
    
  </div>
  <div id="submittedName">Temp text</div>
  	
<script>
function submitName() {
	
	var firstName = document.getElementById("firstName").value;
	var lastName = document.getElementById("lastName").value;
	var name = {
			"firstName" : firstName,
			"lastName" : lastName
	};
	
	var nameData = JSON.stringify(name);
	
	var request = $.ajax({
		type:"post",
		url: "/test/submitName",
		data : nameData,
		contentType : "application/json",
		accept: "application/json"
	});

	request.done(function( msg ) {
		document.getElementById("submittedName").innerHTML=msg.name.firstName + " " + msg.name.lastName;
	});

	request.fail(function( jqXHR, textStatus ) {
		console.log(jqXHR);
		console.log(textStatus);
		alert(jqXHR.responseText);
		alert( "FAIL: " + textStatus );
	});
}
</script>
</body>
</html>